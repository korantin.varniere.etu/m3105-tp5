package fr.univlille.iutinfo.m3105.q3;

import fr.univlille.iutinfo.m3105.modelQ2.Echelle;
import fr.univlille.iutinfo.m3105.modelQ2.Temperature;
import fr.univlille.iutinfo.m3105.viewQ3.TextView;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseButton;
import javafx.stage.Stage;
import javafx.stage.Window;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.testfx.api.FxAssert;
import org.testfx.api.FxRobot;
import org.testfx.framework.junit5.ApplicationExtension;
import org.testfx.framework.junit5.ApplicationTest;
import org.testfx.matcher.control.TextInputControlMatchers;
import org.testfx.service.finder.WindowFinder;
import org.testfx.service.query.NodeQuery;
import org.testfx.util.WaitForAsyncUtils;


/**
 * Thanks to the <code>@ExtendWith(ApplicationExtension.class)</code>, test methods have access to RobotFx
 * The robot allows to manipulate and query the GUI
 */
@ExtendWith(ApplicationExtension.class)
public class TestQ3_3 {

    /**
     * Launches the given main class before running each test
     */
    @BeforeEach
    public void setup() throws Exception {
        ApplicationTest.launch(fr.univlille.iutinfo.m3105.MainQ3.class);
    }

    @Test
    public void test_initial_value_of_Celsius_textField(FxRobot robot) {
        FxAssert.verifyThat(
                queryTextFieldInWindow(robot, celsiusWindow(robot)),
                TextInputControlMatchers.hasText("18.0"));
    }

    @Test
    public void test_initial_value_of_Fahrenheit_textField(FxRobot robot) {
        FxAssert.verifyThat(
                queryTextFieldInWindow(robot, fahrenheitWindow(robot)),
                TextInputControlMatchers.hasText("64.4"));
    }


    @Test
    public void test_plus_button_increments_Celsius_value(FxRobot robot) {
        Window window  = celsiusWindow(robot);

        // .query() executes the NodeQuery and returns the first result
        Button plusButton = queryLabeledNodeInWindow(robot, window, "+").query();

        robot.moveTo(plusButton).clickOn(MouseButton.PRIMARY);
        WaitForAsyncUtils.waitForFxEvents();

        // check new value of TextField in CELSIUS Window
        FxAssert.verifyThat(
                queryTextFieldInWindow(robot, window),
                TextInputControlMatchers.hasText("19.0"));
    }

    @Test
    public void test_plus_button_increments_Fahrenheit_value(FxRobot robot) {
        Window window  = fahrenheitWindow(robot);

        // .query() executes the NodeQuery and returns the first result
        Button plusButton = queryLabeledNodeInWindow(robot, window, "+").query();

        robot.moveTo(plusButton).clickOn(MouseButton.PRIMARY);

        // check new value of TextField in FAHRENHEIT Window
        FxAssert.verifyThat(
                queryTextFieldInWindow(robot, window),
                TextInputControlMatchers.hasText("65.4"));
    }


    /**********************************************************
     *  U T I L I T I E S
     **********************************************************/

    /**
     * Uses the FxRobot to find the window for Celsius
     */
    protected Window celsiusWindow(FxRobot robot) {
        return windowWithTitle(robot, "Celsius");
    }

    /**
     * Uses the FxRobot to find the window for Fahrenheit
     */
    protected Window fahrenheitWindow(FxRobot robot) {
        return windowWithTitle(robot, "Fahrenheit");
    }

    /**
     * Uses the FxRobot to find the window for Newton
     */
    protected Window newtonWindow(FxRobot robot) {
        return windowWithTitle(robot, "Newton");
    }

    /**
     * uses the WindowFinder of the FxRobot to find the (first) window having the <code>expectedTitle</code>
     * Will throw a <code>NoSuchElementException</code> if the window is not found
     */
    protected Window windowWithTitle(FxRobot robot, String expectedTitle) {
        WindowFinder finder = robot.robotContext().getWindowFinder();
        finder.targetWindow(expectedTitle);
        return finder.targetWindow();
    }

    /**
     * Returns a <code>NodeQuery</code> that will filter all <code>TextField</code> Nodes inside the Scene of the <code>window</code>
     * <code>robot.from</code> is used to create an initial NodeQuery
     * <code>NodeQuery.lookup</code> creates another query that searches all nodes matching the lambda inside the children of the first query
     */
    protected NodeQuery queryTextFieldInWindow(FxRobot robot, Window window) {
        return robot.from(((Stage)window).getScene().getRoot()).lookup( (Node n) -> n instanceof TextField);
    }

    /**
     * Returns a <code>NodeQuery</code> that will filter all Nodes inside the Scene of the <code>window</code> with the <code>expectedLabel</code>
     * <code>robot.from</code> is used to create an initial NodeQuery
     * <code>NodeQuery.lookup</code> creates another query that searches all nodes matching the lambda inside the children of the first query
     */
    protected NodeQuery queryLabeledNodeInWindow(FxRobot robot, Window window, String expectedLabel) {
        return robot.from(((Stage)window).getScene().getRoot()).lookup( expectedLabel);
    }
}
