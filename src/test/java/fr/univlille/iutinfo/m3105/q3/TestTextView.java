package fr.univlille.iutinfo.m3105.q3;

import fr.univlille.iutinfo.m3105.modelQ2.Echelle;
import fr.univlille.iutinfo.m3105.modelQ2.Temperature;
import fr.univlille.iutinfo.m3105.viewQ3.TextView;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseButton;
import javafx.stage.Stage;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.testfx.api.FxAssert;
import org.testfx.framework.junit5.ApplicationTest;
import org.testfx.framework.junit5.Start;
import org.testfx.framework.junit5.Stop;
import org.testfx.matcher.base.GeneralMatchers;
import org.testfx.matcher.base.StyleableMatchers;
import org.testfx.matcher.control.TextInputControlMatchers;
import org.testfx.service.query.NodeQuery;

public class TestTextView extends ApplicationTest {

    protected TextView vue = null;
    protected Temperature temp;

    @Start
    public void start(Stage stage) throws Exception {
        // we ignore the main Stage created by ApplicationTest and create our own stage (TextView)

        temp = new Temperature(Echelle.CELSIUS);
        temp.setTemperature(18.0);

        vue = new TextView(temp);
    }

    /**
     * 1st argument of verifyThat(...) is a <code>NodeQuery</code> that will find a TextField in the tested window (there is only one)
     *
     * 2nd argument is a <code>Matcher</code> that checks whether one element (of the previous NodeQuery) has for text "18.0"
     *   The query (1st argument) is such that there should be only one Node matching it
     *   The Matcher assumes the elements returned by the query are TextInputControl (such as a TExtField) which it is, by construction of the query
     */
    @Test
    public void test_initial_value_of_textField() {
        FxAssert.verifyThat(
                queryTextFieldInWindow(),
                TextInputControlMatchers.hasText("18.0"));
    }

    @Test
    public void test_plus_button_increments_value() {
        // from(...) creates an initial NodeQuery from the rootNode in the tested window
        // lookup("+") searches for children Nodes of the rootNode that are Labeled (such as Buttons) and have this label ("+")
        // .query() executes the NodeQuery and returns the first result
        Button plusButton = from(getWindowRootNode()).lookup("+").query();

        moveTo(plusButton).clickOn(MouseButton.PRIMARY);
        // this may be necessary as JavaFX executes in another thread and JUnit thread could be faster than JavaFX thread
        //WaitForAsyncUtils.waitForFxEvents();

        // check new value of TextField
        FxAssert.verifyThat(
                queryTextFieldInWindow(),
                TextInputControlMatchers.hasText("19.0"));
    }

    @Test
    public void test_minus_button_decrements_value() {
        // from(...) creates an initial NodeQuery from the rootNode in the tested window
        // lookup("+") searches for children Nodes of the rootNode that are Labeled (such as Buttons) and have this label ("+")
        // .query() executes the NodeQuery and returns the first result
        Button plusButton = from(getWindowRootNode()).lookup("-").query();

        moveTo(plusButton).clickOn(MouseButton.PRIMARY);
        // this may be necessary as JavaFX executes in another thread and JUnit thread could be faster than JavaFX thread
        //WaitForAsyncUtils.waitForFxEvents();

        // check new value of TextField
        FxAssert.verifyThat(
                queryTextFieldInWindow(),
                TextInputControlMatchers.hasText("17.0"));
    }

    /**********************************************************
     *  U T I L I T I E S
     **********************************************************/

    private Parent getWindowRootNode() {
        return vue.getScene().getRoot();
    }

    /**
     * Returns a <code>NodeQuery</code> that will filter all <code>TextField</code> Nodes inside the Scene of the tested window
     *   <code>from(...)</code> is used to create an initial NodeQuery
     *   <code>lookup(...)</code> creates another query that searches, inside the children of the first query, all nodes matching the lambda
     */
    private NodeQuery queryTextFieldInWindow() {
        return from(getWindowRootNode()).lookup( (Node n) -> n instanceof TextField);
    }

}
