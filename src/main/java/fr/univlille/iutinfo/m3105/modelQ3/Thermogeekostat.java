package fr.univlille.iutinfo.m3105.modelQ3;

import fr.univlille.iutinfo.m3105.modelQ2.Echelle;
import fr.univlille.iutinfo.m3105.modelQ2.Temperature;
import fr.univlille.iutinfo.m3105.utils.Subject;

public class Thermogeekostat extends Subject {
	public static final double DEFAULT_VALUE =  18.0;
	
	protected Temperature wishedTemp;
	
	public Thermogeekostat(Echelle echelle) {
		wishedTemp = new Temperature(echelle);
		wishedTemp.setTemperature(DEFAULT_VALUE);
	}

	public double getWishedTemperature() {
		return wishedTemp.getTemperature();
	}

	public void setWishedTemperature(double val) {
		wishedTemp.setTemperature(val);
		notifyObservers(wishedTemp);
	}

	public void incrementWishedTemperature() {
		wishedTemp.incrementTemperature();
		notifyObservers(wishedTemp);
	}

	public void decrementWishedTemperature() {
		wishedTemp.decrementTemperature();
		notifyObservers(wishedTemp);
	}

}
