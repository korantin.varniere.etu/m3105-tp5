package fr.univlille.iutinfo.m3105.viewTP3;

import fr.univlille.iutinfo.m3105.modelQ2.Temperature;
import javafx.scene.Scene;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class ChooseView extends Stage {
	
	private VBox root = new VBox();
	private HBox celsius = new HBox();
	private HBox fahrenheit = new HBox();
	private HBox kelvin = new HBox();
	private HBox newton = new HBox();
	private HBox rankine = new HBox();

	public ChooseView(Temperature tempC, Temperature tempF, Temperature tempN, Temperature tempK, Temperature tempR) {
		initLabelsAndButtons(tempC, tempF, tempN, tempK, tempR);
		root.getChildren().addAll(celsius, fahrenheit, kelvin, newton, rankine);
		Scene scene = new Scene(root);
		InitChooseView.addEventOnClose(this);
		this.setScene(scene);
		this.setTitle("Choose Temp");
		this.show();
	}

	private void initLabelsAndButtons(Temperature tempC, Temperature tempF, Temperature tempN, Temperature tempK,
			Temperature tempR) {
		InitChooseView.addLabelAndButtonsForTemperature(tempC, celsius, "Celsius");
		InitChooseView.addLabelAndButtonsForTemperature(tempF, fahrenheit, "Fahrenheit");
		InitChooseView.addLabelAndButtonsForTemperature(tempK, kelvin, "Kelvin");
		InitChooseView.addLabelAndButtonsForTemperature(tempN, newton, "Newton");
		InitChooseView.addLabelAndButtonsForTemperature(tempR, rankine, "Rankine");
	}
	
}
