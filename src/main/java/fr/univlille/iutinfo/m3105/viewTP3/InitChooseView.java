package fr.univlille.iutinfo.m3105.viewTP3;

import fr.univlille.iutinfo.m3105.modelQ2.Temperature;
import fr.univlille.iutinfo.m3105.viewQ3.SliderView;
import fr.univlille.iutinfo.m3105.viewQ3.TextView;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

public abstract class InitChooseView {

	public static void addEventOnClose(Stage stage) {
		stage.setOnCloseRequest(new EventHandler<WindowEvent>() {
			@Override
			public void handle(WindowEvent event) {
				Platform.exit();
				System.exit(0);
			}
		});
	}

	public static void addLabelAndButtonsForTemperature(Temperature tempC, HBox celsius, String label) {
		Label cL = new Label(label + " : ");
		Button cT = new Button("TextView");
		addEventCreatingText(tempC, cT);
		Button cS = new Button("SliderView");
		addEventCreatingSlider(tempC, cS);
		celsius.getChildren().addAll(cL, cT, cS);
	}

	public static void addEventCreatingText(Temperature tempC, Button cT) {
		cT.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				new TextView(tempC);
			}
		});
	}

	public static void addEventCreatingSlider(Temperature tempC, Button cS) {
		cS.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				new SliderView(tempC);
			}
		});
	}
	
}
