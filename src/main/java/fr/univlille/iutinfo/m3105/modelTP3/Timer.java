package fr.univlille.iutinfo.m3105.modelTP3;

import fr.univlille.iutinfo.m3105.modelQ2.Temperature;
import fr.univlille.iutinfo.m3105.utils.Observer;
import fr.univlille.iutinfo.m3105.utils.Subject;

public class Timer extends Thread implements Observer {
	
	private static int timeSinceLastUpdate = 0;
	private Temperature temp;
	
	public Timer(Temperature temp ) {
		this.temp = temp;
	}
	
	public void run(){
		while (true) {
			try {
				addOneSecondSinceLastUpdate();
				setTemperatureIfNotUpdated();
			} catch (InterruptedException e) {}
		}
	}

	private void addOneSecondSinceLastUpdate() throws InterruptedException {
		sleep(1000);
		timeSinceLastUpdate++;
	}

	private void setTemperatureIfNotUpdated() {
		if (timeSinceLastUpdate == 5) {
			update(temp);
			temp.setTemperature(18);
		}
	}

	@Override
	public void update(Subject subj) {
		timeSinceLastUpdate = 0;
	}

	@Override
	public void update(Subject subj, Object data) {
		timeSinceLastUpdate = 0;
		((Temperature) (subj)).setTemperature(18);
	}

}
