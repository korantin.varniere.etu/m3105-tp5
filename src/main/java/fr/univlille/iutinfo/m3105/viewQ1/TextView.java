package fr.univlille.iutinfo.m3105.viewQ1;


import fr.univlille.iutinfo.m3105.modelQ1.Thermogeekostat;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;


public class TextView extends AbstractVue {
	protected TextField saisie;

	public TextView(Thermogeekostat temp) {
		super(temp);
	}

	@Override
	protected Region createSceneContent() {
		VBox vb = initVB();
		HBox hbox = initHB();
		saisie = makeTextField();
		addHBoxChildren(hbox); 
		configVB(vb, hbox);
		return vb;
	}

	private void configVB(VBox vb, HBox hbox) {
		vb.getChildren().add( hbox);
		vb.setMaxWidth(Region.USE_PREF_SIZE);
		vb.setPrefWidth(250.0);
	}

	private void addHBoxChildren(HBox hbox) {
		hbox.getChildren().addAll(
				makeButton("-", (e) -> decrementAction()),
				saisie,
				makeButton("+", (e) -> incrementAction()) );
	}

	private HBox initHB() {
		HBox hbox = new HBox();
		hbox.setAlignment(Pos.CENTER);
		hbox.setSpacing(10);
		hbox.setPadding(new Insets(5, 10, 5, 10));
		return hbox;
	}

	private VBox initVB() {
		VBox vb = new VBox();
		vb.setAlignment(Pos.CENTER);
		vb.setPadding(new Insets(5, 10, 5, 10));
		vb.setSpacing(10);
		vb.getChildren().add(new Label( "Température en Celsius"));
		return vb;
	}

	private TextField makeTextField() {
		TextField tf = new TextField();
		tf.setMaxWidth(Region.USE_PREF_SIZE);
		tf.setPrefWidth(70.0);
		tf.setAlignment(Pos.CENTER_RIGHT);
		tf.setOnAction( (e) -> modele.setTemperature(getDisplayedValue()) );
		return tf;
	}

	public double getDisplayedValue() {
		return Double.parseDouble( saisie.getText());
	}

	public void setDisplayedValue(double val) { saisie.setText( String.format("%.1f", val) ); }

}
